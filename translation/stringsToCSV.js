var strings = require('./cloud_strings.js').data;

function objToCSV(obj, prefix) {
    for (key in obj) {
        var val = obj[key];
        if (prefix) key = prefix + '═' + key;
        if (Object.prototype.toString.call(val) === "[object Object]") {
            objToCSV(val, key);
        } else {
            if (Object.prototype.toString.call(val) === "[object Array]") {
                key = '┘' + key;
            }
            val = '"' + val.toString().replace(/"/g, '""') + '"';
            console.log(key + "," + val + ","); 
        }
    }
}

objToCSV(strings);
