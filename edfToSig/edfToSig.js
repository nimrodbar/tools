const fs = require('fs');
const edfdecoder = require("edfdecoder");

var inFile = process.argv[2];

var buffer = fs.readFileSync(inFile).buffer;

function Float32Concat(first, second)
{
    var firstLength = first.length,
    result = new Float32Array(firstLength + second.length);

    result.set(first);
    result.set(second, firstLength);

    return result;
}

var decoder = new edfdecoder.EdfDecoder();
decoder.setInput( buffer );
decoder.decode();
var edf = decoder.getOutput();

var numOfRecords = edf.getNumberOfRecords();
var numOfSignals = 8; //edf.getNumberOfSignals();
var labels=[];
var edfSignals = [];
var outSig = [];

for (var iSig=0;iSig<numOfSignals; iSig++) {
    var signalLabel = edf.getSignalLabel(iSig);
    labels.push(signalLabel);
    console.log(signalLabel);
    edfSignals[iSig] = new Float32Array();
    for (i=0;i<numOfRecords;i++) {
        var aSignal = edf.getPhysicalSignal( iSig, i );
        edfSignals[iSig] = Float32Concat(edfSignals[iSig], aSignal);
    }
}

var edfSignalsLen = edfSignals.length
var maxNumOfSamples = 0;
for (var i=0;i<edfSignalsLen;i++) {
    if (edfSignals[i].length > maxNumOfSamples) maxNumOfSamples = edfSignals[i].length;
}
var duration = maxNumOfSamples * 0.5;
console.log(maxNumOfSamples);

var ecgSig = []
for (i=0;i<maxNumOfSamples;i++) {
    var vector = [];
    for (j=0;j<edfSignalsLen;j++) {
        vector.push(edfSignals[j][i] || 0);
    }
    ecgSig.push(vector);
}

var sigJson = {
"header":{
"formatName":"Beecardia ECG v2",
"startTime":1536068842,
"startTimeZone":180,
"duration":0,
"samplingRate":1000,
"numberOfSamples":0,
"unitsPerMv":328,
"ecgDataBlockSize":0,
"numberOfLeads":0,
"ecgCrc":0,
"leadsIdentificationTable":null,
"recordId":"Rec191A37897B3090CC9A282341252463F8316B0681",
"deviceId":"DevD119BF562B71DECDA9584008141BCF0190469FB1",
"deviceClass":"AtlasSense",
"deviceNumber":"xxx",
"deviceFwVersion":"",
"headerCrc":0,
"electrodesPosition":5,
"swVersion":"2.6.78",
"mobileDeviceInfo":"Amazon KFFOWI 5.1.1",
"signalLabels": labels
},
"ecg": ecgSig,
"footer":{
"duration":duration,
"numberOfSamples": maxNumOfSamples,
"numberOfLeads":edfSignalsLen,
"ecgCrc":0,
}}

var outFile = inFile.replace('.edf', '.sig');

fs.writeFile(outFile, JSON.stringify(sigJson), function(err) {
if(err) {
    return console.log(err);
}
    console.log("The file was saved!");
}); 
