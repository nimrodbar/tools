const fs = require('fs');
const readline = require('readline');

var inFile = process.argv[2];
var numOfSamples = 0;
var duration = 0;
var samplingRate = 1000;
var ecgSig = []
var sigJson = {};
var maxSamples = 300000;

var rl = readline.createInterface({
    input: fs.createReadStream(inFile)
});

rl.on('line', function(line) {
    numOfSamples++;
    var vec = line.split(',');
    vec = [vec[1], vec[2], vec[7], vec[8], vec[9], vec[10], vec[11], vec[12]];
    if (numOfSamples < maxSamples) {
        ecgSig.push(vec);
    }
});

rl.on('close', function(line) {
    console.log(numOfSamples);
    writeOutFile()
});

function writeOutFile() {
    duration = maxSamples * (1/samplingRate) * 1000; 
    prepareJson();

    var outFile = inFile.replace('.txt', '.sig');
        fs.writeFile(outFile, JSON.stringify(sigJson), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    }); 
};

function prepareJson() {
sigJson = {
"header":{
"formatName":"Beecardia ECG v2",
"startTime":1536068842,
"startTimeZone":180,
"duration":0,
"samplingRate": samplingRate,
"numberOfSamples":0,
"unitsPerMv":10379,
"ecgDataBlockSize":0,
"numberOfLeads":0,
"ecgCrc":0,
"leadsIdentificationTable":null,
"recordId":"Rec191A37897B3090CC9A282341252463F8316B0681",
"deviceId":"DevD119BF562B71DECDA9584008141BCF0190469FB1",
"deviceClass":"AtlasSense",
"deviceNumber":"xxx",
"deviceFwVersion":"",
"headerCrc":0,
"electrodesPosition":5,
"swVersion":"2.6.78",
"mobileDeviceInfo":"Amazon KFFOWI 5.1.1",
},
"ecg": ecgSig,
"footer":{
"duration":duration,
"numberOfSamples": numOfSamples,
"numberOfLeads": 8,
"ecgCrc":0,
}}
};
